package com.marcinmilewski.treasure.finder


import spock.lang.Specification

abstract class TreasureFinderTest extends Specification {

    def treasureService

    void "Sample output for sample input should match"() {
        when:
        def paths = treasureService.findPaths(1, 1)

        then:
        paths.get(index).equals(new Path(row, column))

        where:
        index | row | column
        0     | 1   | 1
        1     | 5   | 5
        2     | 1   | 5
        3     | 2   | 1
        4     | 4   | 4
        5     | 3   | 2
        6     | 1   | 3
        7     | 2   | 5
        8     | 4   | 3
    }

    void "Should throw exception if row or column index out of bound of treasure map"() {
        when:
        treasureService.findPaths(row, column)

        then:
        thrown(IndexOutOfBoundsException)

        where:
        row | column
        0   | 2
        2   | 0
        1   | 6
        6   | 1
        -2  | 5
        5   | -2
    }

    void "Should throw exception if path does not lead to treasure"() {
        when:
        treasureService.findPaths(row, column)

        then:
        thrown(TreasureNotFoundException)

        where:
        row | column
        2   | 2
        3   | 1
        2   | 4
        5   | 3
        3   | 3
        4   | 5
        4   | 1
        4   | 2
        2   | 2
    }

    void "Should return correct amount of steps for each start point"() {
        when:
        def paths = treasureService.findPaths(row, column)

        then:
        paths.size() == stepsAmount

        where:
        row | column | stepsAmount
        1   | 1      | 9
        4   | 3      | 1
        2   | 1      | 6
        5   | 5      | 8
        4   | 4      | 5
    }
}

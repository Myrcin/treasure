package com.marcinmilewski.treasure.finder

import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.test.annotation.MicronautTest
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class HelloControllerSpec extends Specification {

    @Inject
    @Client("/")
    RxHttpClient client


    void "Should return 9 paths for start: row-1 column-1"() {
        when:
        HttpRequest request = HttpRequest.GET('/api/v1/treasures/findPath?row=1&column=1')
        List<Path> response = client.toBlocking().retrieve(request, Argument.of(List))

        then:
        response.size() == 9
    }

    void "Should return bad request http status for column index exceeding 6"() {
        when:
        HttpRequest request = HttpRequest.GET('/api/v2/treasures/findPath?row=1&column=6')
        client.toBlocking().retrieve(request, Argument.of(HttpClientResponseException))

        then:
        def e = thrown(HttpClientResponseException)
        e.getStatus() == HttpStatus.BAD_REQUEST
    }
}

package com.marcinmilewski.treasure.finder

class TreasureMap {

    private final List<List<Path>> all

    TreasureMap(List<List<Path>> all) {
        this.all = all
    }

    Path getPath(int row, int column) {
        return all.get(row - 1).get(column - 1)
    }
}

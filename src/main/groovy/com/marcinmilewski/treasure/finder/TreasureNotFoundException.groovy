package com.marcinmilewski.treasure.finder

class TreasureNotFoundException extends RuntimeException {

    TreasureNotFoundException() {
        super("NO TREASURE")
    }
}

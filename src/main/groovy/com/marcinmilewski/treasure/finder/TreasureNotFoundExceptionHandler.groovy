package com.marcinmilewski.treasure.finder

import io.micronaut.context.annotation.Requires
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Produces
import io.micronaut.http.hateoas.JsonError
import io.micronaut.http.server.exceptions.ExceptionHandler

import javax.inject.Singleton

@Produces
@Singleton
@Requires
class TreasureNotFoundExceptionHandler implements ExceptionHandler<TreasureNotFoundException, HttpResponse> {

    @Override
    HttpResponse handle(HttpRequest request, TreasureNotFoundException exception) {
        JsonError body = new JsonError(exception.getMessage())
        return HttpResponse.badRequest().body(body)
    }
}

package com.marcinmilewski.treasure.finder

interface TreasureFinder {

    List<Path> findPaths(int row, int column)
}
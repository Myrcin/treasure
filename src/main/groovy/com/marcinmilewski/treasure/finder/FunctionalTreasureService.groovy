package com.marcinmilewski.treasure.finder

@Singleton
class FunctionalTreasureService implements TreasureFinder {

    List l1 = [55, 14, 25, 52, 21]
    List l2 = [44, 31, 11, 53, 43]
    List l3 = [24, 13, 45, 12, 34]
    List l4 = [42, 22, 43, 32, 41]
    List l5 = [51, 23, 33, 54, 15]

    List<List> all = [l1, l2, l3, l4, l5]

    @Override
    List<Path> findPaths(int row, int column) {
        List<Path> history = new ArrayList()
        while (true) {
            addToPathHistory(history, row, column)
            def value = all.get(row - 1).get(column - 1)
            if (value == 10 * row + column) {
                break
            }
            row = value / 10
            column = value % 10
        }
        return history
    }

    private def addToPathHistory(def history, def row, def column) {
        def path = new Path(row, column)
        if (history.contains(path)) {
            throw new TreasureNotFoundException()
        }
        history.add(path)
    }
}

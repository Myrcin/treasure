package com.marcinmilewski.treasure.finder

class Path {

    final int row
    final int column

    static Path b(int row, int column) {
        return new Path(row, column)
    }

    Path(int row, int column) {
        this.row = row
        this.column = column
    }

    int getRow() {
        return row
    }

    int getColumn() {
        return column
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false
        Path path = (Path) o
        if (column != path.column) return false
        if (row != path.row) return false
        return true
    }

    int hashCode() {
        int result
        result = row
        result = 31 * result + column
        return result
    }
}

package com.marcinmilewski.treasure.finder

@Singleton
class ObjectOrientedTreasureService implements TreasureFinder {

    @Override
    List<Path> findPaths(int row, int column) {
        def map = MapRepository.instance.findTreasureMap()
        def history = new ArrayList()
        checkPath(map, history, new Path(row, column))
        return history
    }

    private checkPath(def map, def history, def start) {
        history.add(start)
        def toPath = map.getPath(start.getRow(), start.getColumn())
        if (start == toPath)
            return
        if (history.contains(toPath))
            throw new TreasureNotFoundException()
        checkPath(map, history, toPath)
    }
}

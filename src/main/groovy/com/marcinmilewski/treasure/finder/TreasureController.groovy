package com.marcinmilewski.treasure.finder

import groovy.transform.CompileStatic
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.micronaut.http.annotation.QueryValue

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

@CompileStatic
@Controller("/api")
class TreasureController {

    @Get("/v1/treasures/findPath")
    @Produces(MediaType.APPLICATION_JSON)
    List<Path> functional(@QueryValue @NotNull @Min(1L) @Max(5L) int row,
                          @QueryValue @NotNull @Min(1L) @Max(5L) int column) {
        FunctionalTreasureService.getInstance().findPaths(row, column)
    }

    @Get("/v2/treasures/findPath")
    @Produces(MediaType.APPLICATION_JSON)
    List<Path> objective(@QueryValue @NotNull @Min(1L) @Max(5L) int row,
                         @QueryValue @NotNull @Min(1L) @Max(5L) int column) {
        ObjectOrientedTreasureService.instance.findPaths(row, column)
    }
}
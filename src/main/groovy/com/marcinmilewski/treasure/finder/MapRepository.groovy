package com.marcinmilewski.treasure.finder

import static com.marcinmilewski.treasure.finder.Path.b

@Singleton
class MapRepository {

    private final static List l1 = [b(5, 5), b(1, 4), b(2, 5), b(5, 2), b(2, 1)]
    private final static List l2 = [b(4, 4), b(3, 1), b(1, 1), b(5, 3), b(4, 3)]
    private final static List l3 = [b(2, 4), b(1, 3), b(4, 5), b(1, 2), b(3, 4)]
    private final static List l4 = [b(4, 2), b(2, 2), b(4, 3), b(3, 2), b(4, 1)]
    private final static List l5 = [b(5, 1), b(2, 3), b(3, 3), b(5, 4), b(1, 5)]

    private final static List<List<Path>> all = [l1, l2, l3, l4, l5]

    static TreasureMap findTreasureMap() {
        return new TreasureMap(all)
    }
}
